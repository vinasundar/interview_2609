﻿using System;
namespace DataLayer.Entity
{
    public class Offer
    {
        public int Id { get; set; }
        public int CountryId { get; set; }
        public string State { get; set; }
        public int? PostCodeId { get; set; }
        public string FullName { get; set; }
    }

    public class Country
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
    }

    public class PostCode
    {
        public int Id { get; set; }
        public string Pcode { get; set; }
        public string Locality { get; set; }
        public string State { get; set; }
        public string Comments { get; set; }
        public string DeliveryOffice { get; set; }
        public string PreSortIndicator { get; set; }
        public string ParcelZone { get; set; }
        public string BSPnumber { get; set; }
        public string BSPname { get; set; }
        public string Category { get; set; }
    }
}
