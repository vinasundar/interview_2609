﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Common;
using DataLayer.Entity;
using Microsoft.EntityFrameworkCore;
using System.Linq;


namespace DataLayer
{
    public abstract class BaseRepository<T> : IDisposable
        where T: class
    {
        protected DbContext context { get; private set; }
        protected DbSet<T> dbSet => context.Set<T>();

        public BaseRepository(DbContext _context)
        {
            context = _context;
        }

        public async Task<ICollection<T>> GetAll()
        {
            try
            {
              var response =  await dbSet.ToListAsync<T>();
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

        public virtual async Task<bool> Create(T entity)
        {
            try
            {
                dbSet.Add(entity);

                var response = await context.SaveChangesAsync();
                return response > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void IDisposable.Dispose()
        {
            context = null;
        }
    }


    public class OfferRepository : BaseRepository<Offer>
    {
        public OfferRepository(DbContext _context) : base(_context)
        {
        }

        public async Task<bool> Create(OfferModel user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }


            var entity = new Offer
            { FullName = user.FullName, CountryId = Convert.ToInt32(user.Country),
                PostCodeId = string.IsNullOrEmpty(user.PostCode) ? default(int): Convert.ToInt32(user.PostCode) ,
                State = user.State };

            return await base.Create(entity);          
        }
    }

    public class CountryRepository : BaseRepository<Country>
    {
        public CountryRepository(DbContext _context) : base(_context)
        {
        }

        public async Task<IList<CountryModel>> GetCountries()
        {
            var countries = await base.GetAll();

            if (countries == null )
            {
                throw new ArgumentNullException( nameof(countries) );
            }

            return countries.ToList()
                .Select(a =>  new CountryModel { Country = a.CountryName, Id = a.CountryId }).ToList();
           
        }
    }

    public class PostCodeRepository : BaseRepository<PostCode>
    {
        public PostCodeRepository(DbContext _context) : base(_context)
        {
        }

        public async Task<IList<PostCodeModel>> GetPostCodes(string state)
        {
            var pcodes = await base.GetAll();

            if (pcodes == null)
            {
                throw new ArgumentNullException(nameof(pcodes));
            }

            return pcodes.Where(a => a.State.ToLower() == state.ToLower())
                .Select(a => new PostCodeModel {  Postcode = a.Pcode, Id = a.Id }).ToList();

        }
    }


}
