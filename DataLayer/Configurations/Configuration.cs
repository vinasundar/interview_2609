﻿using System;
using DataLayer.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.Configurations
{
    public class OfferConfiguration : IEntityTypeConfiguration<Offer>
    {
        public OfferConfiguration()
        {
        }

        public void Configure(EntityTypeBuilder<Offer> builder)
        {
            builder.ToTable("Offers", "dbo");
            builder.Property(a => a.Id).UseIdentityColumn(1, 1);
            builder.HasKey(a => a.Id).HasName("PK_User");
            builder.Property(a => a.State).HasColumnType("varchar(50)").HasMaxLength(50);
            builder.Property(a => a.CountryId).IsRequired();
            builder.Property(a => a.FullName).HasColumnType("varchar(120)").HasMaxLength(120);
        }
    }

    public class CountryConfiguration : IEntityTypeConfiguration<Country>
    {
        public CountryConfiguration()
        {
        }

        public void Configure(EntityTypeBuilder<Country> builder)
        {
            builder.ToTable("Country", "dbo");
            builder.HasKey(a => a.CountryId).HasName("PK_Country");
            builder.Property(a => a.CountryName).HasColumnType("nvarchar(100)")
                .HasMaxLength(100).IsRequired();
            builder.Property(a => a.CountryCode).HasColumnType("nvarchar(5)")
                .HasMaxLength(5).IsRequired();                     
        }
    }

    public class PostCodeConfiguration : IEntityTypeConfiguration<PostCode>
    {
        public PostCodeConfiguration()
        {
        }

        public void Configure(EntityTypeBuilder<PostCode> builder)
        {
            builder.ToTable("Postcodes", "dbo");
            builder.HasKey(a => a.Id).HasName("PK_Postcodes");
            builder.Property(a => a.Pcode).HasColumnType("varchar(50)").HasMaxLength(50);
            builder.Property(a => a.Locality).HasColumnType("varchar(50)").HasMaxLength(50);
            builder.Property(a => a.State).HasColumnType("varchar(50)").HasMaxLength(50);
            builder.Property(a => a.Comments).HasColumnType("varchar(50)").HasMaxLength(50);
            builder.Property(a => a.DeliveryOffice).HasColumnType("varchar(50)").HasMaxLength(50);
            builder.Property(a => a.PreSortIndicator).HasColumnType("varchar(50)").HasMaxLength(50);
            builder.Property(a => a.ParcelZone).HasColumnType("varchar(50)").HasMaxLength(50);
            builder.Property(a => a.BSPnumber).HasColumnType("varchar(50)").HasMaxLength(50);
            builder.Property(a => a.BSPname).HasColumnType("varchar(50)").HasMaxLength(50);
            builder.Property(a => a.Category).HasColumnType("varchar(50)").HasMaxLength(50);            
        }
    }
}
