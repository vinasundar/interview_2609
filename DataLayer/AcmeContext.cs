﻿using System;
using DataLayer.Configurations;
using DataLayer.Entity;
using Microsoft.EntityFrameworkCore;

namespace DataLayer
{
    public class AcmeContext:DbContext
    {
        public DbSet<Offer> Offers { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<PostCode> PostCodes { get; set; }

        public AcmeContext(DbContextOptions<AcmeContext> options) :base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration<Offer>(new OfferConfiguration());
            modelBuilder.ApplyConfiguration<Country>(new CountryConfiguration());
            modelBuilder.ApplyConfiguration<PostCode>(new PostCodeConfiguration());
        }

    }
}
