﻿using System;

namespace Common
{
    public class StaticModel
    {
        public int Id { get; set; }       
    }

    public class CountryModel: StaticModel
    {
        public string Country { get; set; }
    }

    public class StateModel: StaticModel
    {
        public string State { get; set; }
    }

    public class PostCodeModel: StaticModel
    {
        public string Postcode { get; set; }
    }

    public class OfferModel
    {
        public string Country { get; set; }
        public string PostCode { get; set; }
        public string State { get; set; }
        public string FullName { get; set; }
    }
}
