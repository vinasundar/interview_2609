import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { BoardComponent } from './board/board.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AcmeService } from './board/acme.service';
import { SuccessComponent } from './board/success.component';



@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    BoardComponent,
    SuccessComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      { path: '', component: BoardComponent, pathMatch: 'full' },
      { path: 'success', component: SuccessComponent , pathMatch: 'full' }
    ])
  ],
  providers: [AcmeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
