export class Country {
    id: number;
    country: string;
}

export class Offer {
    country: string;
    postCode: string;
    state: string;
    fullName: string;
}

export class PCode {
    id: number;
    postcode: string;
}



export class Error {
    public message: string;
    public errorcode: number;
    public displaymessage: string;
}
