import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn } from '@angular/forms';
import { AcmeService } from './acme.service';
import { Country, PCode, Offer } from './acme.models';
import {Router} from '@angular/router';

const customValidator: ValidatorFn = (fg: FormGroup) => {
  const country = fg.get('country');
  const stateCtrl =  fg.get('state');
 return country !== null  && country.valid &&  country.value === '1'  && stateCtrl !== null &&
  stateCtrl.valid  &&  stateCtrl.value !== null
   ? null : { countrysel: true };
};

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {

  isCountryValid: boolean;
  countries: Country[];
  states: string[];
  pcodes: PCode[];

  appFg: FormGroup;
  constructor(
    private fb: FormBuilder, private svc: AcmeService, private _router: Router
  ) { }

  ngOnInit() {
    this.isCountryValid = false;

    // Load countries 
    this.svc.getCountries().subscribe((data: Country[]) => {
      this.countries = data;
      console.log(data);
     }, (err: Error) => {
        console.log(err);
     }
    );

    this.appFg = this.fb.group({
      country: ['', Validators.required],
      state: [''],
      pcode: [''],
      fname: ['', Validators.required]
    });

    // show state and pcode if country selected is Aus
    this.appFg.controls['country'].valueChanges.subscribe(() => {

      if ( this.appFg.controls['country'].value === '1' ) {
        this.isCountryValid = true;

        if (this.states === undefined) {
          this.svc.getStates().subscribe((data: string[]) => {
            this.states = data;
          },
          (err) => {
            console.log(err);
          });
        }

      } else {
        this.isCountryValid = false;
      }
    });

    // Load pcodes specfic to selected state
    this.appFg.controls['state'].valueChanges.subscribe(() => {
      const selState = this.appFg.controls['state'].value;
      const statecode = selState.split(',')[1].trim();
      // if (this.pcodes === undefined) {
        this.svc.getPCodes(statecode).subscribe((data: PCode[]) => {
          this.pcodes = data;
        },
        (err) => {
          console.log(err);
        });
      // }
    });

  }

  onSubmit(): void {
    if (this.appFg.valid === false) {
      return;
    }

    let _offer = new Offer();
    _offer.country = this.appFg.controls['country'].value;
    _offer.fullName = this.appFg.controls['fname'].value;
    _offer.state = this.appFg.controls['state'].value;
    _offer.postCode = this.appFg.controls['pcode'].value;

    this.svc.offerSubmit(_offer).subscribe((success) => {
      console.log('success');
      this._router.navigate(['/success'], {queryParams: { msg: `offer created `} } );
    },
    (err) => { console.log(err); });
  }

}
