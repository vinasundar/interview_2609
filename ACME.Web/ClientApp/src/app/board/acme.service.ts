import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { of, Observable, throwError } from 'rxjs';
import {first, catchError} from 'rxjs/operators';
import { Country, Error, PCode, Offer } from './acme.models';

export const _url  = of (
  {name: 'countries', url: '/api/countries'},
  {name: 'pcodes', url: '/api/'},
  {name: 'states', url: '/api/States'},
  {name: 'submit', url: '/api/offer/submit'}
 );

export class AcmeHttpClient {
  constructor(protected _http: HttpClient) {}

  protected urlResolve(resolveFor: string): string {
      let url = '';
      _url.pipe(first( a => a.name  === resolveFor ) ).subscribe( x => url = x.url );
      return url;
  }

  protected errorhandler(error: HttpErrorResponse): Observable<Error> {
    let _error = new Error() ;
    _error.displaymessage = 'Error' ;
    _error.message = error.error;
    _error.errorcode = error.status;
    console.log(_error);
    return throwError(_error);
  }

}

@Injectable({
  providedIn: 'root'
})
export class AcmeService extends  AcmeHttpClient {

  constructor(httpClient: HttpClient) { super(httpClient); }

  getCountries(): Observable<Country[] | Error> {
    const _url = this.urlResolve('countries') ;
    return this._http.get<Country[]>(_url,
     {headers: new HttpHeaders({
       'Content-Type': 'application/json'
     })
    }).pipe(catchError( err => this.errorhandler(err) ) );
  }

  getStates(): Observable<string[] | Error> {
    const _url = this.urlResolve('states') ;
    return this._http.get<string[]>(_url,
     {headers: new HttpHeaders({
       'Content-Type': 'application/json'
     })
    }).pipe(catchError( err => this.errorhandler(err) ) );
  }

  getPCodes(state: string): Observable<PCode[] | Error> {
    const _url = this.urlResolve('pcodes') + state + '/pcodes';
    return this._http.get<PCode[]>(_url,
     {headers: new HttpHeaders({
       'Content-Type': 'application/json'
     })
    }).pipe(catchError( err => this.errorhandler(err) ) );
  }

  offerSubmit(_offer: Offer): Observable<boolean | Error> {
    const _url = this.urlResolve('submit') ;
    return this._http.post<boolean>(_url, _offer , {headers: new HttpHeaders({
      'Content-Type': 'application/json', 'Accept': 'application/json'
    })
  }).pipe(catchError( err => this.errorhandler(err) ) );

  }

}


