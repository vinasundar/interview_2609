﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using DataLayer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Web.Controllers
{
    [Route("api/")]
    public class StaticController : Controller
    {
        private AcmeContext context { get;  set; }
        public StaticController(AcmeContext _context)
        {
            context = _context;
        }

        [HttpGet("countries")]
        public async Task<IActionResult> GetCountries()
        {
            using(var repo = new CountryRepository(context))
            {
                IList<CountryModel> countries = await repo.GetCountries();

                if (countries != null && countries.Any())
                {
                    return Ok(countries);
                }
            }
            
            return BadRequest("Error: countries cannot be resolved.");
        }

        [HttpGet("{state}/pcodes")]
        public async Task<IActionResult> GetPostCodes(string state)
        {
            using (var repo = new PostCodeRepository(context))
            {
                IList<PostCodeModel> PCodes = await repo.GetPostCodes(state);              
                return Ok(PCodes);               
            }                      
        }

        [HttpGet("states")]
        public async Task<IActionResult> GetStates()
        {
            IEnumerable<string> PCodes = await Task.Run(() => new string[] {
                "Australian Capital Territory, ACT",
                "New South Wales, NSW",
                "Northern Territory, NT",
                "Queensland, QLD",
                "South Australia, SA ",
                "Tasmania, TAS",
                "Victoria, VIC",
                "Western Australia, WA"
            });

            return Ok(PCodes);
        }
   
    }
}
