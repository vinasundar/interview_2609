﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using DataLayer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Web.Controllers
{
    [Route("api/offer/")]
    public class OfferController : Controller
    {
        private AcmeContext context { get; set; }
        public OfferController(AcmeContext _context)
        {
            context = _context;
        }

        
        [HttpPost("submit")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public async Task<IActionResult> Submit([FromBody] OfferModel offer)
        {
            using (var repo = new OfferRepository(context))
            {
                var isSucessful = await repo.Create(offer);

                if (isSucessful)
                {
                    return Ok();
                }
            }

            return BadRequest("Error: registration failed");
        }

    }
}
